Name : requestSQL
Author : Nicolas Dary
License : GNU General Public License (GPL)
Project statuts : in progress
Version : 1.0.1  29/06/2020
Description EN : This program allows you to perform SQL queries and retrieve the result in CSV format. The information used is stored in an INI file.
Description FR : Ce programme permet d'effectuer des requêtes SQL et de récupérer le résultat sous format CSV. Les informations utilisées sont stockées dans un fichier INI.
Information : 
- The INI file must be placed next to the executed file
- The software can be launched with requestSQL.exe
- The software can also be launched with requestSQL.py (with the command: python requestSQL.py)
- If the software is launched with python, pyodbc must be installed (with the command : pip install pyodbc --user --upgrade)

Gitlab : https://gitlab.com/nicolas.dary/requestsql
Github : https://github.com/nicolasDary/requestSQL

