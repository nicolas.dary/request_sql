#! /usr/bin/python3
# -*- coding: utf-8 -*-
'''
Name ........ : requestSQL.py
Role ........ : Execute SQL request from an ini file
Author ...... : Nicolas & Stephane Dary
Version ..... : V0.4.6 21/11/2020
Licence ..... : GPL


Gestion \\ dans chemin fichier
Gestion unicode utf-8



A Faire :
- Gestion du niveau de log
- Ajout d'un message connexion en cours
Finalisation de l'affichage du traitement en cours (cf labelProcess)
																 

To execute : python requestSQL.py
'''

import tkinter as tk
from tkinter import messagebox
from tkinter import ttk
from tkinter import *
import pyodbc
import csv
import logging
from logging.handlers import RotatingFileHandler
import sys




#Gestion du fichier de log
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s %(levelname)s:%(message)s', datefmt='%d/%m/%Y %H:%M:%S')
file_handler = RotatingFileHandler('log.txt', 'a', 1000000, 1)
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.DEBUG)
logger.addHandler(stream_handler)


#Can manage old and new version of python
try:
    import configparser as cp
except ImportError:
    import ConfigParser as cp

Version = "0.4.6"
TextVersion='V'+Version+' - N&SDY'
Process=''

#Vérification de l'encodate du fichier ini
with open('ExportTotem.ini', "r") as mon_fichier:
    Encodage=mon_fichier.readline()
    if not Encodage.startswith('# -*- coding: utf-8 -*-'):
        logging.info('Attention la première ligne du fichier INI ne contient pas d''information sur son encodage, spécifiez : # -*- coding: utf-8 -*-')
        logging.info('Le risque est un mauvais traitement des caractères spéciaux, accentués,...')
        logging.info('Ligne présente :=>'+Encodage+'<=')
    mon_fichier.close()


#Reading the ini file
file = cp.ConfigParser()
try:
	file.read('ExportTotem.ini','utf-8')
except (cp.Error, Exception) as e:
	logging.critical('Oops, une erreur dans votre fichier de conf : %s', e)
#	print('Oops, une erreur dans votre fichier de conf :', e)
	messagebox.showerror("Error", "Oops, une erreur dans votre fichier de conf"+"\n"+str(e))
	sys.exit(1)

#Gestion du niveau de log
Nivlog=file.get('GENERAL','Log')

Nivlog=Nivlog[0:8].upper()
if Nivlog.startswith('DEBUG'):
	log=logging.DEBUG
	print('Log Niveau Debug')
	file_handler.setLevel(log)
if Nivlog.startswith('INFO'):
	log=logging.INFO
	print('Log Niveau Info')
	file_handler.setLevel(log)
if Nivlog.startswith('WARNING'):
	log=logging.INFO
	print('Log Niveau Warning')
	file_handler.setLevel(log)
if Nivlog.startswith('ERROR'):
	log=logging.INFO
	print('Log Niveau Error')
	file_handler.setLevel(log)
if Nivlog.startswith('CRITICAL'):
	log=logging.INFO
	print('Log Niveau Critical')
	file_handler.setLevel(log)


logging.info('Ouverture de l''application '+TextVersion)
logging.info('Niveau de log : %s', Nivlog)
logging.debug('Encodage : '+sys.getfilesystemencoding())
logging.debug('Version Python : '+sys.version)
Encodage=sys.getfilesystemencoding()

ProcStock=file.get('GENERAL','ProcStock')
ProcStock=ProcStock[0:1]

logging.debug('ProcStock: %s', ProcStock)

#Connection to SQL database
server =  file.get('BDD', 'Serveur')
database =  file.get('BDD', 'Base')
username =  file.get('BDD', 'User')
password = file.get('BDD', 'Pswd')

try:																																		
	cnxn = pyodbc.connect('Driver={SQL Server};SERVER='+ server +';DATABASE='+ database +';UID='+ username +';PWD='+ password+'; autocommit=True')
except pyodbc.Error as ex:
 #   sqlstate = ex.args[0]
    sqlstate = ex.args[1]
    sqlstate = sqlstate.split(".")
#    with open("log.txt", "a") as mon_fichier:
#        mon_fichier.write(sqlstate[-3] +".\n")
#        mon_fichier.close()
    logging.critical(sqlstate[-3]+"\n"+"   Paramètres : "+"\n"+"   Serveur : "+server+"\n"+"   Bdd : "+database+"\n"+"   User : "+username+"\n"+"   Pass : "+password)
    messagebox.showerror("Error", sqlstate[-3]+"\n"+"Paramètres : "+"\n"+"Serveur : "+server+"\n"+"Bdd : "+database+"\n"+"User : "+username+"\n"+"Pass : "+password)
    quit()

cursor = cnxn.cursor()

#Search the number of requests
nbRequests = int(file.get('TRAITEMENTS', 'nbRequests'))
nbRequestsReal = 0
again = True
#Checking the number of requests
while again and nbRequests != nbRequestsReal:
    try:
        #In ini file the id of the request need to be in this format : 1, 2, ..., 10, 11, ...
        value = file.get('TRAITEMENTS', str(nbRequestsReal+1))
        nbRequestsReal = nbRequestsReal +1
    except cp.NoOptionError:
        again = False
        logging.warning('WARNING : Le nombre total de requête ('+nbRequests+')est différent de la requête courante : %s', str(nbRequestsReal))

 
#My application using pyodbc
class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.grid(row=1,column=1,rowspan=10)


def request(nRequest):
    nameFile = file.get('TRAITEMENTS', str(nRequest))
    #nameFile=nameFile.encode("iso-8859-1").decode("utf-8")
    #nameFile=ensure_unicode(nameFile)
    logging.debug('Ouverture : '+nameFile)
#    labelProcess.config(text=nameFile)
    Process=nameFile
    statusbar.config(text=Process)
    logging.info('Process : '+Process)

    try:
        delimiterChoose ='|'
        delimiterChoose =file.get('FICHIER','separateur') 						#Work but weird
        nameFile2=file.get('FICHIER', 'Source')	
        #nameFile2=nameFile2.encode('utf-8')								    #Récupère le chemin du fichier source
        #logging.info('Le chemin source du fichier ini : %s', str(nameFile2))
        if nameFile2.endswith('\\')==True:										#Test la présence d'un \ en fin de chemin
        	nameFile2 = file.get('FICHIER', 'Source') + nameFile
        	#logging.debug('Chemin avec / : '+nameFile2.decode('utf-8'))

        else:
        	nameFile2 = file.get('FICHIER', 'Source') +'\\' +nameFile
        	#nameFile2=nameFile2.encode("iso-8859-1").decode("utf-8")
        	#nameFile2=ensure_unicode(nameFile2)
        	logging.debug('Chemin sans / : '+nameFile2)
        #nameFile2 = file.get('FICHIER', 'Source') + nameFile 					
        #nameFile2 = '..\data\Requests\ '[:-1] + nameFile
        with open(nameFile2, "r") as mon_fichier:
            logging.debug("Fichier source : "+str(nameFile2))
            value = mon_fichier.read() 											#Lecture du fichier de requête
            mon_fichier.close() 												#Fermeture du fichier de requête
        try:
            if value[0:5].upper() == "EXEC " and ProcStock == '1':
                #logging.debug('Exec : %s', str(value[0:5].upper))   #Test la présence d'un \ en fin de chemin
                logging.debug('La requête est une procédure stockée : %s', str(nameFile2))   #Test la présence d'un \ en fin de chemin
                i = 1
                lines = value.split("\n")
                for line in lines:
                    parameters = line.split("@")
                    parameters.pop(0)                                           #Retire le premier élement de la liste
                    nameOutfile = nameFile[:-4]
                    for parameter in parameters: 
                        nameOutfile += parameter
                    nameOutfile=str(nameOutfile).replace(" ","").replace("'","").replace(",","-").replace("/","-")  
                    logging.debug('Execute : %s', str(line))                                #Log
                    logging.debug('Dans fichier : %s', str(nameOutfile))                    #Log
                    #logging.debug('Dans fichier : %s', str(nameOutfile).replace(" ","").replace("'",""))
                    rows = cursor.execute(line)
                    dest = file.get('FICHIER', 'Destination') 							#Récupération du nom de fichier de sortie   
                    #logging.info('Le chemin destination du fichier ini : %s', str(dest))	#Test la présence d'un \ en fin de chemin
                    if dest.endswith('\\')==True:
                        dest = file.get('FICHIER', 'Destination') 
                    else:
                        dest = file.get('FICHIER', 'Destination') +'\\'


                    try: 
                        with open(dest + nameOutfile + '.csv', 'w',newline= '') as csvfile :
                            logging.debug("Fichier destination : "+str(dest + nameOutfile + '.csv'))
                            if cursor.description is not None:                                      #Si le résultat n'est pas vide
                                print('Cursor is not none')
                                writer = csv.writer(csvfile, delimiter=delimiterChoose)
                                writer.writerow([x[0] for x in cursor.description])  			# column headers
                                for row in rows:                                            #gestion des lignes
                                    writer.writerow(row)
                            else:
                                logging.error("Problème de Print dans la ProcStock ou de colonne sans nom")

                    except:
                        logging.error("Impossible d'érire le fichier de destination : "+str(dest + nameOutfile + '.csv'))
                    i = i + 1

            else:
                rows = cursor.execute(value)
                dest = file.get('FICHIER', 'Destination') 							#Récupération du nom de fichier de sortie   
                #logging.info('Le chemin destination du fichier ini : %s', str(dest))	#Test la présence d'un \ en fin de chemin
                if dest.endswith('\\')==True:
                    dest = file.get('FICHIER', 'Destination') 
                else:
                    dest = file.get('FICHIER', 'Destination') +'\\' 
                with open(dest + nameFile[:-4] + '.csv', 'w',newline= '') as csvfile :
                    logging.debug("Fichier destination : "+str(dest + nameFile[:-4] + '.csv'))
                    writer = csv.writer(csvfile, delimiter=delimiterChoose)
                    try:
                        writer.writerow([x[0] for x in cursor.description])  			# column headers
                    except TypeError:
                        logging.error("Certaine colonne n'ont pas de nom")
                    for row in rows:
                        writer.writerow(row)
        except pyodbc.Error as err:
            sqlstate=err.args[1]
            logging.warning("WARNING : Problème SQL : "+"\n"+sqlstate)
            #logging.warning("WARNING : Résultat vide")
        # except None:
        #     #sqlstate=err.args[1]
        #     logging.warning("WARNING : Résultat vide")


    except FileNotFoundError:
        logging.warning("WARNING : Le fichier " + str(nRequest) +" n'existe pas.")        

    #Process='Fin de traitement'
    logging.info('Fin de Process : '+Process)
    #labelProcess.config(text=Process)
    statusbar.config(text='Fin de '+Process)
	

def allRequest():
    for i in range(1,nbRequestsReal+1):
        request(i)

class infoBulle(tk.Toplevel):
	def __init__(self,parent=None,texte='',temps=100):
		tk.Toplevel.__init__(self,parent,bd=1,bg='black')
		self.tps=temps
		self.parent=parent
		self.withdraw()
		self.overrideredirect(1)
		self.transient()     
		l=tk.Label(self,text=texte,bg="white",justify='left')
		l.update_idletasks()
		l.pack()
		l.update_idletasks()
		self.tipwidth = l.winfo_width()
		self.tipheight = l.winfo_height()
		self.parent.bind('<Enter>',self.delai)
		self.parent.bind('<Button-1>',self.efface)
		self.parent.bind('<Leave>',self.efface)
	def delai(self,event):
		self.action=self.parent.after(self.tps,self.affiche)
	def affiche(self):
		self.update_idletasks()
		posX = self.parent.winfo_rootx()+self.parent.winfo_width()
		posY = self.parent.winfo_rooty()+self.parent.winfo_height()
		if posX + self.tipwidth > self.winfo_screenwidth():
			posX = posX-self.winfo_width()-self.tipwidth
		if posY + self.tipheight > self.winfo_screenheight():
			posY = posY-self.winfo_height()-self.tipheight
		self.geometry('+%d+%d'%(posX,posY))
		self.deiconify()
        #logging.debug('Position : '+str(posX)+' - '+str(posY))
	def efface(self,event):
		self.withdraw()
		self.parent.after_cancel(self.action)

        
root = tk.Tk()
dic = {}
infobulles = {}
row = 1
column = 1
for i in range(1,nbRequestsReal+1):
    msg = file.get('TRAITEMENTS', str(i))
    #msg = msg.encode("iso-8859-1").decode("utf-8")
    key = 'button' + str(i)
    dic[key] = tk.Button(root, text = str(i), command = lambda i=i : request(i))   
    dic[key].grid(row=row, column=column)
    infobulles[key] = infoBulle(parent=dic[key],texte=msg)
    if i % 4 == 0:
        row = row +1
        column = 1
    else:
        column = column +1

#Getion des boutons
buttonAll = tk.Button(root, text = "ALL", command=lambda x="": allRequest())
buttonAll.grid(row=row+1, column=column, columnspan=2, rowspan=2,
               sticky=tk.W+tk.E+tk.N+tk.S, padx=5, pady=5)
#labelProcess=Label(root, text =Process)															#Gestion du process en cours dans l'écran
#labelProcess.grid(row=row+2, column=1)
#labelProcess.grid(row=row+1, column=1, columnspan=2, rowspan=2,
#               sticky=tk.W+tk.E+tk.N+tk.S, padx=5, pady=5)	 
labelVersion = Label(root, text =TextVersion)                                                       #Affichage de la version dans l'écran
labelbdd = Label(root, text =server+' - '+database)                                                              #Affichage du nom de la base dans l'écran
statusbar = tk.Label(root, text="", bd=1, relief=tk.SUNKEN, anchor=tk.W)

#Positionnement dans la fenêtre
labelbdd.grid(row=row+4, column=1, columnspan=2, rowspan=2,
               sticky=tk.W+tk.E+tk.N+tk.S, padx=5, pady=5)
labelVersion.grid(row=row+6, column=1, columnspan=2, rowspan=2,
               sticky=tk.W+tk.E+tk.N+tk.S, padx=5, pady=5)


#Launch of my application
root.geometry('300x450')
root.title("RequestSQL v" + Version)
app = Application(master=root)
app.mainloop()
