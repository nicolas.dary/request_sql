#!/usr/bin/python
# -*- coding: utf-8 -*-
# Python 3
#https://python.jpvweb.com/python/mesrecettespython/doku.php?id=cx_freeze


from cx_Freeze import setup, Executable

exe=Executable(
        script="requestSQL.py",
        base="Win32Gui",                                    #Appli Windows avec fenetre
        icon="iconmonstr-database-16-240.ico",              #Icone du fichier .exe
        targetName="RequestSQL",                            #Non du fichier .exe
        copyright="Mercuria"                                #Copyright du fichier
        )
includefiles=[]
includes=[]
excludes=["asyncio","concurrent","ctypes","distutils","email","html","http","lib2to3","multiprocessing","pydoc_data","test","unittest","urllib","xml","xmlrpc"]
packages=[]

setup(
        name = "RequestSQL.exe",
        version = "0.4.1",
        description = "Outil d'execution de requetes SQL en chaine",
        author = "N&SDY",
        options = {'build_exe': {'excludes':excludes,'packages':packages,'include_files':includefiles}},
        executables = [exe]
        )
